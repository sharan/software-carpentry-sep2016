# SWC September 2016
## Day 1: 16:30-18:00
- variables
- literals, constants
- data structures
  - strings
  - numeric (plus math operators)
  - lists
  - dictionaries
- file handling
- for
- if/else

## Day 2: 09:00-10:30
- function definitions (reusing the file reading code from Day 1)
- docstrings, good naming practice
- import statements, namespaces
- examples of using imported stuff
  - pandas -> read tabular file, plot
  - numpy, scipy
- list of commonly-used libraries

## Day 3: 10:45-12:00
- exercises
  - flow chart templates
  - debugging, 'fragile' code

